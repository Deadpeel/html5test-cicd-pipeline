// Fill out your copyright notice in the Description page of Project Settings.


#include "HTML5TestGameModeBase.h"
#include "TheButton.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"

void AHTML5TestGameModeBase::BeginPlay()
{
	if (IsValid(ClickableButton))
	{
		TheButton = Cast<UTheButton>(CreateWidget(GetWorld(), ClickableButton));

		if (TheButton)
		{
			TheButton->AddToViewport();
		}
	}

	APlayerController* Playerul = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	Playerul->bShowMouseCursor = true;


}

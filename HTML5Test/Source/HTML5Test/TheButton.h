// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TheButton.generated.h"

/**
 * 
 */
UCLASS()
class HTML5TEST_API UTheButton : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* Button1;
	
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnButtonClicked();
};

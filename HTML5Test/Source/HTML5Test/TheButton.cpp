// Fill out your copyright notice in the Description page of Project Settings.


#include "TheButton.h"
#include "Math/Color.h"
#include "Components/Button.h"


void UTheButton::NativeConstruct()
{
	if (Button1)
	{
		Button1->OnClicked.AddDynamic(this, &UTheButton::OnButtonClicked);
	}
}

void UTheButton::OnButtonClicked()
{
	Button1->SetColorAndOpacity(FLinearColor::Green);
}

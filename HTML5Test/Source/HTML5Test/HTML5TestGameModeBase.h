// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HTML5TestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HTML5TEST_API AHTML5TestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, Category = "Class Types")
		TSubclassOf<UUserWidget> ClickableButton;

	UPROPERTY(VisibleInstanceOnly)
		class UTheButton* TheButton;

	virtual void BeginPlay() override;
};
